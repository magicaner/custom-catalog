<?php
namespace Magic\CustomCatalog\Model\ResourceModel\Product;

use Magento\Eav\Model\Entity\Collection\VersionControl\AbstractCollection;

/**
 * Class Collection
 * @package Magic\CustomCatalog\Model\ResourceModel\Product
 */
class Collection extends AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Magic\CustomCatalog\Model\Product::class,
            \Magic\CustomCatalog\Model\ResourceModel\Product::class
        );
    }

    /**
     * Let do something before add loaded item in collection
     *
     * @param \Magento\Framework\DataObject $item
     * @return \Magento\Framework\DataObject
     */
    protected function beforeAddLoadedItem(\Magento\Framework\DataObject $item)
    {
        return $item;
    }
}