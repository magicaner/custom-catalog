<?php
/**
 * Custom Catalog Product EAV additional attribute resource collection
 */
namespace Magic\CustomCatalog\Model\ResourceModel\Attribute;

class Collection extends \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection
{
    /**
     * Default attribute entity type code
     *
     * @var string
     */
    protected $_entityTypeCode = 'magic_custom_catalog_product';


}
