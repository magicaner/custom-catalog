<?php

namespace Magic\CustomCatalog\Model;

use Magento\Framework\Api\Search\SearchResult;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;
use Magic\CustomCatalog\Api\ProductInterface;
use Magic\CustomCatalog\Api\ProductRepositoryInterface;
use Magic\CustomCatalog\Model\ResourceModel\Product\Collection;
use Magic\CustomCatalog\Model\ResourceModel\Product\CollectionFactory;
use Magic\CustomCatalog\Model\Product;
use Magic\CustomCatalog\Model\ProductFactory;

/**
 * Product Repository.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ProductRepository implements ProductRepositoryInterface
{

    /**
     * @var ProductFactory
     */
    private $productFactory;
    /**
     * @var ResourceModel\Product
     */
    private $resourceModel;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Api\SearchResultsFactory
     */
    private $searchResultsFactory;


    /**
     * ProductRepository constructor.
     * @param \Magic\CustomCatalog\Model\ProductFactory $productFactory
     * @param ResourceModel\Product $resourceModel
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchResultsFactory $searchResultsFactory
     */
    public function __construct(
        ProductFactory $productFactory,
        ResourceModel\Product $resourceModel,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchResultsFactory $searchResultsFactory
    ) {

        $this->productFactory = $productFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @inheritdoc
     */
    public function get($sku, $storeId = null)
    {
        /** @var Product $product */
        $product = $this->productFactory->create();
        $productId = $this->resourceModel->getIdBySku($sku);

        if (!$productId) {
            throw new NoSuchEntityException(
                __("The product that was requested doesn't exist. Verify the product and try again.")
            );
        }

        if ($storeId !== null) {
            $product->setData('store_id', $storeId);
        }
        $product->load($productId);
        return $product;
    }

    /**
     * @inheritdoc
     */
    public function getById($productId, $storeId = null)
    {
        /** @var Product $product */
        $product = $this->productFactory->create();

        if ($storeId !== null) {
            $product->setData('store_id', $storeId);
        }

        $product->load($productId);

        if (!$product->getId()) {
            throw new NoSuchEntityException(
                __("The product that was requested doesn't exist. Verify the product and try again.")
            );
        }
        return $product;
    }

    /**
     * @inheritdoc
     */
    public function getByVpn($vpn, $storeId = null)
    {
        /** @var Product $product */
        $product = $this->productFactory->create();
        $productId = $this->resourceModel->getIdByVpn($vpn);
        if ($storeId !== null) {
            $product->setData('store_id', $storeId);
        }

        $product->load($productId);

        if (!$product->getId()) {
            throw new NoSuchEntityException(
                __("The product that was requested doesn't exist. Verify the product and try again.")
            );
        }
        return $product;
    }

    /**
     * @param ProductInterface $product
     * @return \Magic\CustomCatalog\Api\ProductInterface
     */
    public function save(\Magic\CustomCatalog\Api\ProductInterface $product)
    {
        $this->resourceModel->save($product);
        return $this->get($product->getSku(), false, $product->getStoreId());
    }

    /**
     * @inheritdoc
     */
    public function delete(\Magic\CustomCatalog\Api\ProductInterface $product)
    {
        try {
            $this->resourceModel->delete($product);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('The "%1" product couldn\'t be removed.', $product->getSku())
            );
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById($sku)
    {
        $product = $this->get($sku);
        return $this->delete($product);
    }

    /**
     * @inheritdoc
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $collection->load();

        /** @var \Magento\Framework\Api\SearchResults $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}
