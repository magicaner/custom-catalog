<?php

namespace Magic\CustomCatalog\Model;

use Magento\Framework\Model\AbstractModel;
use Magic\CustomCatalog\Api\ProductInterface;

/**
 * Class Product
 * @package Magic\CustomCatalog\Model
 */
class Product extends AbstractModel implements ProductInterface
{
    const ENTITY = 'magic_custom_catalog_product';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(\Magic\CustomCatalog\Model\ResourceModel\Product::class);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->getData('store_id');
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->getData('sku');
    }

    /**
     * @return string
     */
    public function getVpn()
    {
        return $this->getData('vpn');
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->getData('product_id');
    }

    /**
     * @return string
     */
    public function getCopyWriteInfo()
    {
        return $this->getData('copy_write_info');
    }

    /**
     * @inheritDoc
     */
    public function getGroup()
    {
        return $this->getData('group');
    }


    /**
     * @inheritDoc
     */
    public function setStoreId($storeId)
    {
        $this->setData('store_id', $storeId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        $this->setData('sku', $sku);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setVpn($vpn)
    {
        $this->setData('vpn', $vpn);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setProductId($productId)
    {
        $this->setData('product_id', $productId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setCopyWriteInfo($copyWriteInfo)
    {
        $this->setData('copy_write_info', $copyWriteInfo);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setGroup($groupId)
    {
        $this->setData('group', $groupId);
        return $this;
    }
}
