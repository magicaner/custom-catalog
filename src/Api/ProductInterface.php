<?php
namespace Magic\CustomCatalog\Api;

/**
 * Interface ProductInterface
 * @package Magic\CustomCatalog\Api
 */
interface ProductInterface
{
    const TABLE_NAME = 'magic_custom_catalog_product_entity';
    const ID = 'entity_id';
    const ENTITY_TYPE = 'magic_custom_catalog_product';

    const FIELD_GROUP = 'group';
    /**
     * @return int
     */
    public function getStoreId();

    /**
     * @return string
     */
    public function getSku();

    /**
     * @return string
     */
    public function getVpn();

    /**
     * @return string
     */
    public function getProductId();

    /**
     * @return string
     */
    public function getCopyWriteInfo();

    /**
     * @return id
     */
    public function getGroup();


    /**
     * @param int $storeId
     * @return ProductInterface
     */
    public function setStoreId($storeId);

    /**
     * @param string $sku
     * @return ProductInterface
     */
    public function setSku($sku);

    /**
     * @param string $vpn
     * @return ProductInterface
     */
    public function setVpn($vpn);

    /**
     * @param string $pdoductId
     * @return ProductInterface
     */
    public function setProductId($pdoductId);

    /**
     * @param string $copyWriteInfo
     * @return ProductInterface
     */
    public function setCopyWriteInfo($copyWriteInfo);

    /**
     * @param mixed $groupId
     * @return ProductInterface
     */
    public function setGroup($groupId);

}
