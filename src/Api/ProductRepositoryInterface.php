<?php

namespace Magic\CustomCatalog\Api;


/**
 * Interface AddressMetadataInterface
 * @package Magento\Customer\Api
 */
interface ProductRepositoryInterface
{
    const FIELD_PRODUCT_ID = 'product_id';
    const FIELD_SKU = 'sku';
    const FIELD_VPN = 'vpn';
    const FIELD_COPY_WRITE_INFO = 'copy_write_info';

    /**
     * @param int|string $sku
     * @param null $storeId
     * @return \Magic\CustomCatalog\Api\ProductInterface
     */
    public function get($sku, $storeId = null);

    /**
     * @param int $productId
     * @param null $storeId
     * @return \Magic\CustomCatalog\Api\ProductInterface
     */
    public function getById($productId, $storeId = null);

    /**
     * @param string $vpn
     * @param null $storeId
     * @return \Magic\CustomCatalog\Api\ProductInterface
     */
    public function getByVpn($vpn, $storeId = null);

    /**
     * @param ProductInterface $product
     * @return \Magic\CustomCatalog\Api\ProductInterface
     */
    public function save(\Magic\CustomCatalog\Api\ProductInterface $product);

    /**
     * @param ProductInterface $product
     * @return boolean
     */
    public function delete(\Magic\CustomCatalog\Api\ProductInterface $product);

    /**
     * @param string $sku
     * @return boolean
     */
    public function deleteById($sku);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
