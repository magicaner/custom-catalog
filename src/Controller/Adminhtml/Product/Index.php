<?php

namespace Magic\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;

/**
 * Class Index
 * @package Magic\CustomCatalog\Contoller\Adminhtml\Index
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magic_CustomCatalog::all';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $pageFactory;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $pageResult = $this->pageFactory->create();
        $pageResult->getConfig()->getTitle()->prepend((__('Custom Catalog')));
        return $pageResult;
    }
}
