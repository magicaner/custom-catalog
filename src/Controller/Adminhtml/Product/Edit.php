<?php

namespace Magic\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManager;

/**
 * Class Edit
 * @package Magic\CustomCatalog\Contoller\Adminhtml\Index
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magic_CustomCatalog::all';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $pageFactory;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var \Magic\CustomCatalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory,
        Registry $registry,
        \Magic\CustomCatalog\Model\ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $productId = (int) $this->getRequest()->getParam('id');

        $product = $this->productRepository->getById($productId, $storeId);
        $this->registry->register('current_custom_catalog_product', $product);


        $pageResult = $this->pageFactory->create();
        $pageResult->getConfig()->getTitle()->prepend((__('Custom Catalog - Edit Product #' . $product->getId())));
        return $pageResult;
    }
}
