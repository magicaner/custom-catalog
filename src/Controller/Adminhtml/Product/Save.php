<?php

namespace Magic\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magic\CustomCatalog\Model\Product;
use Magic\CustomCatalog\Model\ProductFactory;
use Magic\CustomCatalog\Model\ProductRepository;

/**
 * Class Save
 * @package Magic\CustomCatalog\Contoller\Adminhtml\Index
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magic_CustomCatalog::all';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        Registry $registry,
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        ManagerInterface $messageManager
    ) {

        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {

        $storeId = (int) $this->getRequest()->getParam('store_id', 0);
        $productId = (int) $this->getRequest()->getParam('entity_id');

        if ($productId) {
            $product = $this->productRepository->getById($productId, $storeId);
        } else {
            $product = $this->productFactory->create();
        }

        $product->setStoreId($storeId);
        $product->setSku($this->getRequest()->getParam('sku'));
        $product->setVpn($this->getRequest()->getParam('vpn'));
        $product->setProductId($this->getRequest()->getParam('product_id'));
        $product->setCopyWriteInfo($this->getRequest()->getParam('copy_write_info'));

        try {
            $this->productRepository->save($product);
            $this->messageManager->addSuccessMessage('Product has been saved successfully');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        if ($this->getRequest()->getParam('back')) {

            $params = [];
            if ($product->getId()) {
                $params['id'] = $product->getId();
            }

            if ($storeId) {
                $params['store'] = $storeId;
            }
            $resultRedirect->setPath(
                '*/*/' . $this->getRequest()->getParam('back'),
                $params
            );
        }
        return $resultRedirect;
    }
}
