<?php

namespace Magic\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManager;
use \Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class InlineEditAction
 *
 * @package Magic\CustomCatalog\Controller\Adminhtml\Product
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magic_CustomCatalog::all';

    /**
     * @var \Magic\CustomCatalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * InlineEditAction constructor.
     *
     * @param Action\Context $context
     * @param \Magic\CustomCatalog\Model\ProductRepository $productRepository
     * @param JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        Action\Context $context,
        \Magic\CustomCatalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
    ) {
        parent::__construct($context);
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                                            'messages' => [__('Please correct the data sent.')],
                                            'error' => true,
                                        ]);
        }

        foreach (array_keys($postItems) as $productId) {
            $product = $this->productRepository->getById($productId);
            $this->dataObjectHelper->populateWithArray(
                $product,
                $postItems[$productId],
                Magic\CustomCatalog\Api\ProductInterface::class
            );

            $this->saveProduct($product);
        }

        return $resultJson->setData([
            'messages' => $this->getErrorMessages(),
            'error' => $this->isErrorExists()
        ]);
    }

    /**
     * @param $product
     */
    protected function saveProduct($product)
    {
        try {
            $this->productRepository->save($product);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }
    /**
     * Get array with errors
     *
     * @return array
     */
    protected function getErrorMessages()
    {
        $messages = [];
        foreach ($this->getMessageManager()->getMessages()->getItems() as $error) {
            $messages[] = $error->getText();
        }
        return $messages;
    }

    /**
     * Check if errors exists
     *
     * @return bool
     */
    protected function isErrorExists()
    {
        return (bool)$this->getMessageManager()->getMessages(true)->getCount();
    }
}
