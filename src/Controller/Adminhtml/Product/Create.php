<?php

namespace Magic\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magic\CustomCatalog\Model\Product;
use Magic\CustomCatalog\Model\ProductFactory;

/**
 * Class Create
 * @package Magic\CustomCatalog\Contoller\Adminhtml\Index
 */
class Create extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magic_CustomCatalog::all';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $pageFactory;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        Registry $registry,
        ProductFactory $productFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        $this->productFactory = $productFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var Product $product */
        $product = $this->productFactory->create();
        $this->registry->register('current_custom_catalog_product', $product);

        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Magento_Catalog::custom_catalog');
        $resultPage->getConfig()->getTitle()->prepend(__('Products'));
        $resultPage->getConfig()->getTitle()->prepend(__('New Product'));



        return $resultPage;
    }
}
