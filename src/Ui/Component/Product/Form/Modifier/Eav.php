<?php
namespace Magic\CustomCatalog\Ui\Component\Product\Form\Modifier;

use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Eav\Model\Entity\Type;
use Magento\Eav\Model\Entity\TypeFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magic\CustomCatalog\Api\ProductInterface;
use Magic\CustomCatalog\Model\Attribute\ScopeOverriddenValue;
use Magic\CustomCatalog\Model\Product;
use Magento\Ui\DataProvider\Mapper\FormElement as FormElementMapper;
use Magento\Ui\DataProvider\Mapper\MetaProperties as MetaPropertiesMapper;


/**
 * Class Eav
 */
class Eav implements ModifierInterface
{
    const SOURCE_NAME = 'custom_catalog_product';
    const SORT_ORDER_MULTIPLIER = 10;

    const SCOPE_STORE = 0;
    const SCOPE_GLOBAL = 1;
    const SCOPE_WEBSITE = 2;
    const KEY_IS_GLOBAL = 'is_global';

    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var AttributeCollectionFactory
     */
    private $collectionFactory;
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var AttributeFactory
     */
    private $attributeFactory;

    private $canDisplayUseDefault = [];

    /**
     * @var ScopeOverriddenValue
     */
    private $scopeOverriddenValue;
    /**
     * @var FormElementMapper
     */
    private $formElementMapper;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var TypeFactory
     */
    private $entityTypeFactory;
    /**
     * @var \Magento\Eav\Model\AttributeRepository
     */
    private $attributeRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * Eav constructor.
     * @param Registry $registry
     * @param AttributeCollectionFactory $collectionFactory
     * @param ArrayManager $arrayManager
     * @param AttributeFactory $attributeFactory
     * @param ScopeOverriddenValue $scopeOverriddenValue
     * @param FormElementMapper $formElementMapper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Registry $registry,
        AttributeCollectionFactory $collectionFactory,
        ArrayManager $arrayManager,
        AttributeFactory $attributeFactory,
        ScopeOverriddenValue $scopeOverriddenValue,
        FormElementMapper $formElementMapper,
        StoreManagerInterface $storeManager,
        TypeFactory $entityTypeFactory,
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {

        $this->registry = $registry;
        $this->collectionFactory = $collectionFactory;
        $this->arrayManager = $arrayManager;
        $this->attributeFactory = $attributeFactory;
        $this->scopeOverriddenValue = $scopeOverriddenValue;
        $this->formElementMapper = $formElementMapper;
        $this->storeManager = $storeManager;
        $this->entityTypeFactory = $entityTypeFactory;
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return Product
     */
    protected function getProduct()
    {
        return $this->registry->registry('current_custom_catalog_product');
    }

    /**
     * @inheritdoc
     * @since 101.0.0
     */
    public function modifyData(array $data)
    {
        $product = $this->getProduct();
        if ($product->getId()) {
            $data[$product->getId()]['general'] = $product->getData();
        }
        return $data;
    }

    /**
     * @inheritdoc
     * @since 101.0.0
     */
    public function modifyMeta(array $meta)
    {
        $criteria = $this->searchCriteriaBuilder->create();
        $attributesCollection = $this->attributeRepository
            ->getList(ProductInterface::ENTITY_TYPE, $criteria);

        $meta['general']['children'] = [];
        $attributesMeta = [];
        $groupCode = 'product';
        $i = 0;

        /** @var \Magento\Eav\Model\Entity\Attribute $attribute */
        foreach ($attributesCollection->getItems() as $attribute) {
            $i++;
            $attributesMeta[$attribute->getAttributeCode()] = $this->setupAttributeMeta($attribute, $groupCode, $i);
        }

        $meta['general']['children'] = $attributesMeta;

        return $meta;
    }

    /**
     * @param Attribute $attribute
     * @param $groupCode
     * @param $sortOrder
     * @return array
     */
    public function setupAttributeMeta(Attribute $attribute, $groupCode, $sortOrder)
    {
        $attributeCode = $attribute->getAttributeCode();
        $configPath = 'arguments/data/config';

        $meta = $this->arrayManager->set($configPath, [], [
            'dataType' => $attribute->getFrontendInput(),
            'formElement' => $this->getFormElementsMapValue($attribute->getFrontendInput()),
            'visible' => $attribute->getIsVisible(),
            'required' => $attribute->getIsRequired(),
            'notice' => $attribute->getNote() === null ? null : __($attribute->getNote()),
            'default' => (!$this->isProductExists()) ? $this->getAttributeDefaultValue($attribute) : null,
            'label' => __($attribute->getDefaultFrontendLabel()),
            'code' => $attributeCode,
            'scopeLabel' => $this->getScopeLabel($attribute),
            'globalScope' => $this->isScopeGlobal($attribute),
            'sortOrder' => $sortOrder * self::SORT_ORDER_MULTIPLIER,
        ]);

        if ($this->canDisplayUseDefault($attribute)) {
            $meta = $this->arrayManager->merge($configPath, $meta, [
                'service' => [
                    'template' => 'ui/form/element/helper/service',
                ]
            ]);
        }

        if (!$this->arrayManager->exists($configPath . '/componentType', $meta)) {
            $meta = $this->arrayManager->merge($configPath, $meta, [
                'componentType' => Field::NAME,
            ]);
        }

        $meta = $this->addUseDefaultValueCheckbox($attribute, $meta);

        return $meta;
    }

    /**
     * Returns attribute default value, based on db setting or setting in the system configuration.
     *
     * @param Attribute $attribute
     * @return null|string
     */
    private function getAttributeDefaultValue(Attribute $attribute)
    {
        if ($attribute->getAttributeCode() === 'page_layout') {
            $defaultValue = $this->scopeConfig->getValue(
                'web/default_layouts/default_product_layout',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $this->storeManager->getStore()
            );
            $attribute->setDefaultValue($defaultValue);
        }
        return $attribute->getDefaultValue();
    }

    /**
     * Whether attribute can have default value
     *
     * @param Attribute $attribute
     * @return bool
     */
    private function canDisplayUseDefault(Attribute $attribute)
    {
        $attributeCode = $attribute->getAttributeCode();
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->getProduct();

        if (isset($this->canDisplayUseDefault[$attributeCode])) {
            return $this->canDisplayUseDefault[$attributeCode];
        }

        return $this->canDisplayUseDefault[$attributeCode] = (
            ($attribute->getIsGlobal() != self::SCOPE_GLOBAL)
            && $product
            && $product->getId()
            && $product->getStoreId()
        );
    }

    /**
     * Adds 'use default value' checkbox.
     *
     * @param Attribute $attribute
     * @param array $meta
     * @return array
     */
    private function addUseDefaultValueCheckbox(Attribute $attribute, array $meta)
    {
        $canDisplayService = $this->canDisplayUseDefault($attribute);
        if ($canDisplayService) {
            $meta['arguments']['data']['config']['service'] = [
                'template' => 'ui/form/element/helper/service',
            ];

            $meta['arguments']['data']['config']['disabled'] = !$this->scopeOverriddenValue->containsValue(
                ProductInterface::class,
                $this->getProduct(),
                $attribute->getAttributeCode(),
                $this->getStore()->getId()
            );
        }
        return $meta;
    }

    /**
     * Retrieve scope label
     *
     * @param Attribute $attribute
     * @return \Magento\Framework\Phrase|string
     */
    private function getScopeLabel(Attribute $attribute)
    {
        if ($this->storeManager->isSingleStoreMode()
            || $attribute->getFrontendInput() === AttributeInterface::FRONTEND_INPUT
        ) {
            return '';
        }

        switch ($attribute->getIsGlobal()) {
            case self::SCOPE_GLOBAL:
                return __('[GLOBAL]');
            case self::SCOPE_WEBSITE:
                return __('[WEBSITE]');
            case self::SCOPE_STORE:
                return __('[STORE VIEW]');
        }

        return '';
    }


    /**
     * Check if attribute scope is global.
     *
     * @param ProductAttributeInterface $attribute
     * @return bool
     */
    private function isScopeGlobal($attribute)
    {
        return $attribute->getIsGlobal() === self::SCOPE_GLOBAL;
    }

    /**
     * Retrieve form element
     *
     * @param string $value
     * @return mixed
     */
    private function getFormElementsMapValue($value)
    {
        $valueMap = $this->formElementMapper->getMappings();

        return $valueMap[$value] ?? $value;
    }

    /**
     * Check is product already new or we trying to create one
     *
     * @return bool
     */
    private function isProductExists()
    {
        return (bool) $this->getProduct()->getId();
    }

    /**
     * @return Store
     */
    private function getStore()
    {
        $store = $this->registry->registry('current_store');
        if (!$store) {
            $store = $this->storeManager->getDefaultStoreView();
            $this->registry->register('current_store', $store, true);
        }

        return $store;
    }
}
