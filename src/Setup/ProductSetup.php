<?php
namespace Magic\CustomCatalog\Setup;

use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Setup\Context;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductSetup extends EavSetup
{
    /**
     * EAV configuration
     *
     * @var Config
     */
    protected $eavConfig;

    /**
     * Init
     *
     * @param ModuleDataSetupInterface $setup
     * @param Context $context
     * @param CacheInterface $cache
     * @param CollectionFactory $attrGroupCollectionFactory
     * @param Config $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        Context $context,
        CacheInterface $cache,
        CollectionFactory $attrGroupCollectionFactory,
        Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($setup, $context, $cache, $attrGroupCollectionFactory);
    }

    /**
     * Retrieve default entities: customer, customer_address
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefaultEntities()
    {
        $entities = [
            'magic_custom_catalog_product' => [
                'entity_model' => \Magic\CustomCatalog\Model\ResourceModel\Product::class,
                'attribute_model' => \Magento\Eav\Model\Attribute::class,
                'table' => 'magic_custom_catalog_product_entity',
                'increment_model' => \Magento\Eav\Model\Entity\Increment\NumericValue::class,
                'entity_attribute_collection' => \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class,
                'additional_attribute_table' => 'magic_custom_catalog_eav_attribute',
                'attributes' => [
                    'product_id' => [
                        'type' => 'static',
                        'label' => 'Unique identifier of a product',
                        'input' => 'text',
                        'sort_order' => 10,
                        'position' => 10,
                        'adminhtml_only' => 1,
                        'required' => true,
                        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    ],
                    'sku' => [
                        'type' => 'static',
                        'label' => 'SKU',
                        'input' => 'text',
                        'sort_order' => 10,
                        'position' => 10,
                        'adminhtml_only' => 1,
                        'required' => true,
                        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    ],
                    'vpn' => [
                        'type' => 'static',
                        'label' => 'Vendor Product Number',
                        'input' => 'text',
                        'sort_order' => 30,
                        'adminhtml_only' => 1,
                        'required' => true,
                        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    ],
                    'copy_write_info' => [
                        'type' => 'varchar',
                        'label' => 'Copy write information',
                        'input' => 'textarea',
                        'sort_order' => 20,
                        'adminhtml_only' => 1,
                        'required' => true,
                        'global' => ScopedAttributeInterface::SCOPE_STORE,
                    ],

                ],
            ],
        ];
        return $entities;
    }

    /**
     * Gets EAV configuration
     *
     * @return Config
     */
    public function getEavConfig()
    {
        return $this->eavConfig;
    }
}
