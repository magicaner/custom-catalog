<?php

namespace Magic\CustomCatalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magic\CustomCatalog\Setup\ProductSetup;
use Magic\CustomCatalog\Setup\ProductSetupFactory;

class DefaultProductAttributes implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var ProductSetupFactory
     */
    private $setupFactory;

    public function __construct(
        ProductSetupFactory $setupFactory,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->setupFactory = $setupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function apply()
    {
        /** @var \Magic\CustomCatalog\Setup\ProductSetup $setup */
        $setup = $this->setupFactory->create(['setup' => $this->moduleDataSetup]);
        $setup->installEntities();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
