<?php

namespace Magic\CustomCatalog\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Type;
use Magento\Eav\Model\Entity\TypeFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magic\CustomCatalog\Api\ProductInterface;
use Magic\CustomCatalog\Setup\ProductSetup;
use Magic\CustomCatalog\Setup\ProductSetupFactory;

class AddProductGroupAttribute implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var ProductSetupFactory
     */
    private $setupFactory;

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory
     */
    private $entityTypeFactory;

    /**
     * AddProductGroupAttribute constructor.
     *
     * @param ProductSetupFactory $setupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param TypeFactory $entityTypeFactory
     */
    public function __construct(
        ProductSetupFactory $setupFactory,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        TypeFactory $entityTypeFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->setupFactory = $setupFactory;
        $this->entityTypeFactory = $entityTypeFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function apply()
    {
        /** @var \Magic\CustomCatalog\Setup\ProductSetup $setup */
        $setup = $this->setupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityType = $this->entityTypeFactory->create()->loadByCode(\Magic\CustomCatalog\Api\ProductInterface::ENTITY_TYPE);
        $setup->addAttribute($entityType->getId(), ProductInterface::FIELD_GROUP, [
            'type' => 'static',
            'label' => 'Product Group',
            'input' => 'select',
            'sort_order' => 10,
            'position' => 10,
            'adminhtml_only' => 1,
            'required' => true,
            'global' => ScopedAttributeInterface::SCOPE_STORE,
            'option' => [
                'value' => [
                    'wholesale' => [
                        '0' => 'Wholesale',
                        '1' => 'Wholesale',
                    ],
                    'retail' => [
                        '0' => 'Default',
                        '1' => 'Default',
                    ]
                ]
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
